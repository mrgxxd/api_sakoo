'use strict'

const co = require('co')

const config = require('../../config')
const databaseConnection = config.databaseConnection

const FCM = require('fcm-node')
const serverKey = 'AAAAk3Z-2rI:APA91bGYNqd6KwnEhLKtttTC6NKFQJRm9mKl7MuoV-I60aO3YdJIYqBwAUBOgwZCTKBmaK-Vgqt6xNom9jMVNuwBr5Ag3gXeLLTSk4luLBkuv9wMFuFDiaESXNc3rkRfBWDthVwIRyXy' //put your server key here
const fcm = new FCM(serverKey)

const sendOneNotification = (token, title, body, id, type) => {
  return co(function * sendOneNotificationQuery () {

    let message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: token,
            collapse_key: 'your_collapse_key',

            notification: {
                title: title,
                body: body,
                sound : "default"
            },

            data: {  //you can send only notification or only data(or include both)
                title: title,
                body: body,
                sound : "default",
                message : body,
                key: id,
                type: type
            }
        };

    return new Promise ((resolve, reject) => {
      fcm.send(message, function(err, response){
          if (err) {
              reject(err)
              console.log("Something has gone wrong!");
          } else {
              resolve(response)
              console.log("Successfully sent with response: ", response);
          }
      })
    })
  })
}

const insertNotificationUser = (title, body, typeId, type, id) => {
  return co(function * insertNotificationQuery () {
    let sqlString = `INSERT INTO notifikasi_user (title, body, type_id, type, date, user_id) VALUES('${title}','${body}','${typeId}','${type}',NOW(),'${id}')`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const insertNotificationUserHome = (title, body, typeId, type, id, warna) => {
  return co(function * insertNotificationQuery () {
    let sqlString = `INSERT INTO notifikasi_user_home (title, body, type_id, type, date, user_id, status_proses) VALUES('${title}','${body}','${typeId}','${type}',NOW(),'${id}','${warna}')`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const updateNotificationUserHome = (title, body, typeId, type, id, warna) => {
  return co(function * updateNotificationUserHomeQuery () {
    let sqlString = `UPDATE notifikasi_user_home SET title = '${title}', body = '${body}', type = '${type}', date = NOW(), user_id = '${id}', status_proses = '${warna}' WHERE type_id = '${typeId}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const sendAlertToAdmin = (id_tipe, tipe, deskripsi, tanggal) => {
  return co(function * getAllQuery () {
    let sqlString = `INSERT INTO alert(id_tipe,tipe,deskripsi,tanggal) VALUES ('${id_tipe}','${tipe}','${deskripsi}','${tanggal}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getIdAlert = (id_tipe, tipe, deskripsi, tanggal) => {
  return co(function * getIdAlertQuery () {
    let sqlString = `SELECT * FROM alert`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getNotificationById = (id_user) => {
  return co(function * getNotificationByIdQuery () {
    let sqlString = `SELECT * FROM notifikasi_user WHERE user_id = ${id_user} ORDER BY id DESC`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getHomeNotifById = (id) => {
  return co(function * getNotificationByIdQuery () {
    let sqlString = `SELECT * FROM notifikasi_user_home WHERE user_id = ${id} ORDER BY id DESC LIMIT 5`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getByTypeId = (id) => {
  return co(function * getNotificationByIdQuery () {
    let sqlString = `SELECT * FROM notifikasi_user_home WHERE type_id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}



module.exports = {
  sendOneNotification,
  insertNotificationUser,
  insertNotificationUserHome,
  updateNotificationUserHome,
  getNotificationById,
  getHomeNotifById,
  getByTypeId
}
