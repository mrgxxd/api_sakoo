'use strict'

const co = require('co')

const config = require('../../config')
const databaseConnection = config.databaseConnection

const tableMaster = 'toko'
//const tableParent = 'kategori_barang'

const login = (username, password, mode) => {
  return co(function * loginQuery () {
    let sqlString = ""
    if(mode == "no_hp"){
      sqlString = `SELECT * FROM ${tableMaster} WHERE no_hp = '${username}' AND password = '${password}'`
    }else{
      sqlString = `SELECT * FROM ${tableMaster} WHERE email = '${username}' AND password = '${password}'`
    }
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const newLogin = (no_hp, newCode) => {
  return co(function * loginQuery () {
    let sqlString = `UPDATE ${tableMaster} SET code_verifikasi = '${newCode}' WHERE no_hp = '${no_hp}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const register = (username, password, email, no_hp, nama_toko) => {
  return co(function * loginQuery () {
    let sqlString = `INSERT INTO ${tableMaster} (username, password, email, tanggal_register, no_hp, nama) VALUES ('${username}', '${password}', '${email}', NOW(), '${no_hp}', '${nama_toko}')`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const newRegister = (username, nama_toko, no_hp, newCode) => {
  return co(function * loginQuery () {
    let sqlString = `INSERT INTO ${tableMaster} (username, nama, tanggal_register, no_hp, code_verifikasi) VALUES ('${username}', '${nama_toko}', NOW(), '${no_hp}', '${newCode}')`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const updateRegToken = (regId, id, token) => {
  return co(function * updateRegTokenQuery () {
    let sqlString = `UPDATE ${tableMaster} SET reg_id = '${regId}', token = '${token}' WHERE id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const updatePassword = (id, password) => {
  return co(function * updateRegTokenQuery () {
    let sqlString = `UPDATE ${tableMaster} SET password = '${password}' WHERE id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const updateProfile = (id, username, email, nama, no_hp, alamat, foto, bank, rekening, namarekening, lokasiMarkup) => {
  return co(function * updateRegTokenQuery () {
    let sqlString = `UPDATE ${tableMaster} SET username = '${username}', email = '${email}', nama = '${nama}', no_hp = '${no_hp}', alamat = '${alamat}', foto = '${foto}', bank = '${bank}', rekening = '${rekening}', nama_rekening = '${namarekening}', lokasi_markup = '${lokasiMarkup}' WHERE id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getByUsername = (username) => {
  return co(function * getByUsernameQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} WHERE username = '${username}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getByEmail = (email) => {
  return co(function * getByUsernameQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} WHERE email = '${email}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getByPhone = (no_hp) => {
  return co(function * getByUsernameQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} WHERE no_hp = '${no_hp}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getByEmailandCode = (email, myCode) => {
  return co(function * getByUsernameQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} WHERE email = '${email}' code_verifikasi = '${myCode}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getById = (id) => {
  return co(function * getByUsernameQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} WHERE id = '${id}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getByPassword = (pass) => {
  return co(function * getByUsernameQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} WHERE password = '${pass}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getBank = () => {
  return co(function * getByUsernameQuery () {
    let sqlString = `SELECT * FROM bank`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getMarkUp = () => {
  return co(function * getMarkUpQuery () {
    let sqlString = `SELECT * FROM harga_markup`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getUserMarkUp = (id) => {
  return co(function * getMarkUpQuery () {
    let sqlString = `SELECT harga_markup.markup FROM harga_markup JOIN toko ON harga_markup.id = toko.lokasi_markup WHERE toko.id = '${id}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getMarkUpById = (id) => {
  return co(function * getMarkUpQuery () {
    let sqlString = `SELECT markup FROM harga_markup WHERE id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}




module.exports = {
  login,
  newLogin,
  register,
  newRegister,
  updateRegToken,
  updatePassword,
  updateProfile,
  getByUsername,
  getByEmail,
  getByPhone,
  getByEmailandCode,
  getById,
  getByPassword,
  getBank,
  getMarkUp,
  getUserMarkUp,
  getMarkUpById
}
