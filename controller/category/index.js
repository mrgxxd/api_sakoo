'use strict'

const co = require('co')

const config = require('../../config')
const databaseConnection = config.databaseConnection

const tableMaster = 'kategori_barang'
const tableParent = 'ecommerce'

const getAll = () => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} ORDER BY nama`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

module.exports = {
  getAll
}
