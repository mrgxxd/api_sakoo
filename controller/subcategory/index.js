'use strict'

const co = require('co')

const config = require('../../config')
const databaseConnection = config.databaseConnection

const tableMaster = 'sub_kategori_barang'
const tableParent = 'kategori_barang'

const getAll = () => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM ${tableMaster}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getByCategory = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM ${tableMaster} WHERE id_kategori = ${id} ORDER BY nama`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

module.exports = {
  getAll,
  getByCategory
}
