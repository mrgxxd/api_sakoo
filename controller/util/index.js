'use strict'

const co = require('co')

const config = require('../../config')
const databaseConnection = config.databaseConnection

const getKontak = (id) => {
  return co(function * loginQuery () {
    let sqlString = `SELECT admin.kontak_admin FROM admin JOIN toko ON admin.id_admin = toko.id_admin WHERE toko.id = '${id}'`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getVersion = () => {
  return co(function * getVersionQuery () {
    let sqlString = `SELECT * FROM version`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

module.exports = {
  getKontak,
  getVersion
}
