'use strict'

const co = require('co')
const asyncLoop = require('node-async-loop');

const config = require('../../config')
const databaseConnection = config.databaseConnection

const tableMaster = 'barang'
const tableFoto = 'foto_tambahan'
const tableUpload = 'upload_barang'
//const tableParent = 'kategori_barang'

const tambahBarang = (id, subkategori, foto, nama, deskripsi, harga, stok, berat, totalHarga, volume, bahan, merk, statusBarang) => {
  return co(function * tambahBarangQuery () {
    let sqlString = `INSERT INTO ${tableMaster} (id_toko, nama, id_subkategori, stok, stok_ecommerce, harga_satuan, foto, deskripsi, tanggal_upload, berat, harga_markup, volume, bahan, merk, status_baru_bekas) VALUES ('${id}', '${nama}','${subkategori}', '${stok}', '${stok}', '${harga}', '${foto}', '${deskripsi}', NOW(), '${berat}', '${totalHarga}', '${volume}', '${bahan}', '${merk}', '${statusBarang}')`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const editBarang = (id, subkategori, foto, nama, deskripsi, harga, stok, berat, totalHarga, volume, bahan, merk) => {
  return co(function * tambahBarangQuery () {
    let sqlString = `UPDATE ${tableMaster} SET nama = '${nama}', id_subkategori = '${subkategori}', stok = '${stok}', stok_ecommerce = '${stok}', tanggal_update = NOW(), harga_satuan = '${harga}', harga_markup = '${totalHarga}', volume = '${volume}', bahan = '${bahan}', merk = '${merk}', foto = '${foto}', deskripsi = '${deskripsi}', berat = '${berat}' WHERE id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}


const getAllBarang = () => {
  return co(function * tambahBarangQuery () {
    let sqlString = `SELECT max(id) AS max FROM ${tableMaster}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}


  const getByTambah = (subkategori, nama, foto, deskripsi, harga, stok) => {
    return co(function * getByTambahQuery () {
      let sqlString = `SELECT * FROM ${tableMaster} WHERE id_subkategori = '${subkategori}' AND nama = '${nama}' AND foto = '${foto}' AND stok = '${stok}' AND harga_satuan = '${harga}'`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getById = (id) => {
    return co(function * getByTambahQuery () {
      let sqlString = `SELECT * FROM ${tableMaster} WHERE id = ${id}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getUploadBarangById = (id) => {
    return co(function * getByTambahQuery () {
      let sqlString = `SELECT upload_barang.id_ecommerce AS id_ecommerce, upload_barang.id_status AS status_ecommerce, barang.status_penyebab_unlisting AS status_penyebab_unlisting FROM upload_barang JOIN barang ON upload_barang.id_barang = barang.id WHERE barang.id = ${id}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getByUserId = (id) => {
    return co(function * getByUserIdQuery () {
      let sqlString = `SELECT * FROM ${tableMaster} WHERE id_toko = ${id} AND status_aktif = 1`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

const tambahFoto = (id, foto) => {
  return co(function * tambahBarangQuery () {
    let sqlString = `INSERT INTO ${tableFoto} (id_barang, foto) VALUES ('${id}', '${foto}')`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const hapusFoto = (id) => {
  return co(function * tambahBarangQuery () {
    let sqlString = `DELETE FROM foto_tambahan WHERE id_barang = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

  const tambahUploadBarang = (id, idE) => {
    return co(function * tambahUploadBarangQuery () {
      let sqlString = `INSERT INTO ${tableUpload} (id_barang, id_ecommerce, id_status) VALUES ('${id}','${idE}',1)`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getListing = (id) => {
    return co(function * getByTambahQuery () {
      let sqlString = `SELECT upload_barang.id_barang AS id_barang, barang.nama AS nama_barang, ecommerce.nama AS nama_ecommerce, barang.stok AS stok, upload_barang.url AS url, barang.foto AS foto FROM upload_barang JOIN barang ON upload_barang.id_barang = barang.id JOIN toko ON barang.id_toko = toko.id JOIN ecommerce ON upload_barang.id_ecommerce = ecommerce.id WHERE upload_barang.id_status =3 AND barang.stok >0 AND toko.id =${id} ORDER BY barang.tanggal_update DESC , barang.tanggal_upload DESC`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getUnListing = (id) => {
    return co(function * getByTambahQuery () {
      let sqlString = `SELECT upload_barang.id_barang AS id_barang, barang.nama AS nama_barang, ecommerce.nama AS nama_ecommerce, barang.stok AS stok, barang.foto AS foto, upload_barang.id_status AS status FROM upload_barang JOIN barang ON upload_barang.id_barang = barang.id JOIN toko ON barang.id_toko = toko.id JOIN ecommerce ON upload_barang.id_ecommerce = ecommerce.id WHERE (upload_barang.id_status =3 AND barang.stok =0 AND toko.id =${id} AND ecommerce.id != 1) OR (upload_barang.id_status =1 AND toko.id =${id} AND ecommerce.id != 1) ORDER BY barang.tanggal_update DESC , barang.tanggal_upload DESC `
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getListingUnListing = (id, search, filter, sort, index) => {
    return co(function * getListingUnListingQuery () {
      let sqlString = ""

      sqlString = `SELECT DISTINCT barang.id AS id_barang, barang.nama AS nama, barang.deskripsi AS deskripsi, barang.harga_satuan AS harga, barang.stok AS stok, barang.foto AS foto, barang.tanggal_upload AS tanggal_upload, barang.tanggal_update AS tanggal_update, GREATEST(barang.tanggal_upload, COALESCE(barang.tanggal_update, 0)) AS datemax, barang.tanggal_listing AS tanggal_listing, barang.id_subkategori AS id_subkategori, barang.volume AS volume, barang.bahan AS bahan, barang.merk AS merk, sub_kategori_barang.nama AS nama_subkategori, barang.berat AS berat, status_penyebab_unlisting.status AS status_unlisting FROM barang JOIN upload_barang ON barang.id = upload_barang.id_barang JOIN ecommerce ON ecommerce.id = upload_barang.id_ecommerce JOIN sub_kategori_barang ON sub_kategori_barang.id = barang.id_subkategori JOIN status_penyebab_unlisting ON barang.status_penyebab_unlisting = status_penyebab_unlisting.id WHERE barang.id_toko=${id} AND barang.status_aktif = 1`

      if(search != ""){
        sqlString = sqlString + ` AND barang.nama LIKE '%${search}%'`
      }

      if(filter != ""){
        if(filter == "listing"){
          sqlString = sqlString + ` AND upload_barang.id_status = 3 AND barang.stok > 0`
        }else if(filter == "unlisting"){
          sqlString = sqlString + ` AND ((upload_barang.id_status = 3 AND barang.stok = 0 AND ecommerce.id NOT IN (1,3,4)) OR (upload_barang.id_status = 1 AND ecommerce.id NOT IN (1,3,4)))`
        }
      }

      if(sort != ""){
        if(sort == "newupdate"){
          sqlString = sqlString + `  ORDER BY barang.tanggal_update DESC`
        }else if(sort == "lastupdate"){
          sqlString = sqlString + ` ORDER BY barang.tanggal_update ASC`
        }else if(sort == "name"){
          sqlString = sqlString + ` ORDER BY barang.nama ASC`
        }else if(sort == "maxharga"){
          sqlString = sqlString + ` ORDER BY barang.harga_satuan DESC`
        }else if(sort == "minharga"){
          sqlString = sqlString + ` ORDER BY barang.harga_satuan ASC`
        }
        sqlString = sqlString + `, barang.id DESC LIMIT ${index}, 10`
      }else{
        sqlString = sqlString + ` ORDER BY datemax DESC LIMIT ${index}, 10`
      }

      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getBarangByUser = (id, search, status, sort, index) => {
    return co(function * getListingUnListingQuery () {
      let sqlString = ""

      sqlString = `SELECT DISTINCT barang.id AS id_barang, barang.nama AS nama_barang, barang.harga_satuan AS harga_barang, barang.stok AS stok_barang, barang.foto AS foto_barang, barang.tanggal_upload AS tanggal_upload, barang.tanggal_update AS tanggal_update, barang.tanggal_listing AS tanggal_listing, status_penyebab_unlisting.status AS status_penyebab_unlisting FROM barang JOIN upload_barang ON barang.id = upload_barang.id_barang JOIN ecommerce ON ecommerce.id = upload_barang.id_ecommerce JOIN sub_kategori_barang ON sub_kategori_barang.id = barang.id_subkategori JOIN status_penyebab_unlisting ON barang.status_penyebab_unlisting = status_penyebab_unlisting.id WHERE barang.id_toko=${id} AND barang.status_aktif = 1`

      if(search != ""){
        sqlString = sqlString + ` AND barang.nama LIKE '%${search}%'`
      }

      if(status != ""){
        if(status == "listing"){
          sqlString = sqlString + ` AND upload_barang.id_status = 3 AND barang.stok > 0`
        }else if(status == "unlisting"){
          sqlString = sqlString + ` AND ((upload_barang.id_status = 3 AND barang.stok = 0 AND ecommerce.id NOT IN (1,3,4)) OR (upload_barang.id_status = 1 AND ecommerce.id NOT IN (1,3,4)))`
        }
      }

      if(sort != ""){
        if(sort == "newupdate"){
          sqlString = sqlString + `  ORDER BY barang.tanggal_update DESC`
        }else if(sort == "lastupdate"){
          sqlString = sqlString + ` ORDER BY barang.tanggal_update ASC`
        }else if(sort == "name"){
          sqlString = sqlString + ` ORDER BY barang.nama ASC`
        }else if(sort == "maxharga"){
          sqlString = sqlString + ` ORDER BY barang.harga_satuan DESC`
        }else if(sort == "minharga"){
          sqlString = sqlString + ` ORDER BY barang.harga_satuan ASC`
        }
        sqlString = sqlString + `, barang.id DESC LIMIT ${index}, 10`
      }else{
        sqlString = sqlString + ` ORDER BY barang.tanggal_upload DESC , barang.tanggal_update DESC LIMIT ${index}, 10`
      }

      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getEcommerce = (id) => {
    return co(function * getEcommerceQuery () {
      let sqlString = `SELECT ecommerce.nama AS nama_ecommerce, upload_barang.url AS url, upload_barang.id_status AS status FROM ecommerce JOIN upload_barang ON ecommerce.id = upload_barang.id_ecommerce WHERE upload_barang.id_barang = ${id}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getFotoTambahan = (id) => {
    return co(function * getFotoTambahanQuery () {
      let sqlString = `SELECT foto FROM foto_tambahan WHERE id_barang = ${id}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const disticntBarang = (barang) => {
    return co(function * getByTambahQuery () {
      return new Promise ((resolve, reject) => {
        let flags = []
        let output = []
        asyncLoop(barang, function(item, next){
          if(flags[item.id_barang]){
          }else {
            flags[item.id_barang] = true
            output.push(item)
          }
          next()
        }, function(){
          resolve(output)
        })
      })
    })
  }

  const addStockAfterDelete = (idBarang, stok) => {
    return co(function * getByTambahQuery () {
      let sqlString = `UPDATE barang SET stok = ${stok}, stok_ecommerce = ${stok} WHERE id = ${idBarang}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const updateEcommerceAfterAddStock = (idBarang, status) => {
    return co(function * getByTambahQuery () {
      let sqlString = `UPDATE update_stok SET id_status = 1, id_status_penyebab = ${status}  WHERE id_barang = ${idBarang}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const hapusBarang = (id) => {
    return co(function * hapusBarangQuery () {
      let sqlString = `UPDATE barang SET status_aktif = 0, stok = 0, stok_ecommerce = 0 WHERE id = ${id}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const afterHapusBarang = (id) => {
    return co(function * afterhapusBarangQuery () {
      let sqlString = `INSERT INTO log_hapus_barang (timestamp, id_barang) VALUES (NOW(), '${id}')`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const afterEditBarang = (id, field) => {
    return co(function * afterhapusBarangQuery () {
      let sqlString = `INSERT INTO log_edit_barang (timestamp, id_barang, field) VALUES (NOW(), '${id}', '${field}')`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const updateEcommerceAfterSameStock = (idBarang) => {
    return co(function * getByTambahQuery () {
      let sqlString = `UPDATE update_stok SET id_status = 3  WHERE id_barang = ${idBarang}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const editHargaMarkUp = (idToko, hargaMarkup) => {
    return co(function * getByTambahQuery () {
      let sqlString = `UPDATE barang SET harga_markup = hargaMarkup, tanggal_update = NOW() WHERE id_toko = ${idToko}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const updateBarangAfterEdit = (idBarang) => {
    return co(function * getByTambahQuery () {
      let sqlString = `UPDATE upload_barang SET id_status = 1 WHERE id_barang = ${idBarang}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }

  const getUpdateStock = (idBarang) => {
    return co(function * getByTambahQuery () {
      let sqlString = `SELECT * FROM update_stok WHERE id_barang = ${idBarang}`
      return new Promise ((resolve, reject) => {
        databaseConnection.query(sqlString, function(err, rows, fields) {
            if (!err){
              resolve(rows)
            }else{
              reject(err)
            }
        })
      })
    })
  }




module.exports = {
  getAllBarang,
  tambahBarang,
  editBarang,
  getByTambah,
  tambahFoto,
  hapusFoto,
  getListing,
  getUnListing,
  getListingUnListing,
  getBarangByUser,
  getEcommerce,
  getFotoTambahan,
  disticntBarang,
  tambahUploadBarang,
  addStockAfterDelete,
  updateEcommerceAfterAddStock,
  hapusBarang,
  afterHapusBarang,
  afterEditBarang,
  updateEcommerceAfterSameStock,
  getById,
  getUploadBarangById,
  getByUserId,
  updateBarangAfterEdit,
  editHargaMarkUp,
  getUpdateStock
}
