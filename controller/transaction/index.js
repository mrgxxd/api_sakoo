'use strict'

const co = require('co')

const config = require('../../config')
const databaseConnection = config.databaseConnection

const tableMaster = 'detail_transaksi'
const tableParent1 = 'transaksi'
const tableParent2 = 'barang'
const tableToko = 'toko'

const getAll = () => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM ${tableMaster}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getAktif = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT transaksi.id_trx FROM transaksi JOIN transaksi_seller ON transaksi.id_transaksi = transaksi_seller.id_transaksi WHERE transaksi_seller.id_toko = ${id} AND transaksi.status_transaksi IN (1,2,3) `
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getMyTransaction = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT detail_transaksi.id_detail_transaksi AS id_transaksi, detail_transaksi.keterangan AS keterangan_barang, barang.harga_satuan AS harga, detail_transaksi.jumlah_pembelian AS jumlah, detail_transaksi.keterangan AS keterangan, detail_transaksi.nomor_resi AS nomor_resi, detail_transaksi.id_status AS status , detail_transaksi.tanggal_action AS tanggal_action, transaksi_seller.biaya_kirim_seller AS biaya_kirim, transaksi.keterangan AS keterangan_transaksi, transaksi.no_hp AS no_hp_buyer, transaksi.jasa_pengiriman AS jasa_pengiriman, transaksi.asuransi AS asuransi, transaksi.no_trx AS oreder_no, transaksi.tanggal_pemesanan AS tanggal_pemesanan, transaksi.alamat_pengiriman AS alamat, barang.nama AS nama , barang.foto AS foto, barang.id AS id_barang FROM detail_transaksi JOIN transaksi_seller ON detail_transaksi.id_transaksi_seller = transaksi_seller.id JOIN barang ON detail_transaksi.id_barang = barang.id JOIN transaksi ON transaksi_seller.id_transaksi = transaksi.id_transaksi WHERE transaksi_seller.id =${id} AND transaksi.complete = 1`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getTheTransaction = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT transaksi.status_transaksi AS status, status_transaksi.status AS text_status, transaksi_seller.id AS id_transaksi, transaksi_seller.biaya_kirim_seller AS biaya_kirim, transaksi.no_trx AS oreder_no, transaksi.buyer AS buyer, transaksi.tanggal_pemesanan AS tanggal_pemesanan, transaksi.alamat_pengiriman AS alamat FROM transaksi_seller JOIN transaksi ON
    transaksi_seller.id_transaksi = transaksi.id_transaksi JOIN status_transaksi ON transaksi.status_transaksi = status_transaksi.id WHERE transaksi_seller.id_toko =${id} AND transaksi.status_transaksi NOT IN(4, 5) AND transaksi.complete = 1 ORDER BY transaksi.tanggal_pemesanan DESC`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getTheTransactionHistory = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT transaksi.status_transaksi AS status, status_transaksi.status AS text_status, transaksi_seller.id AS id_transaksi, transaksi_seller.biaya_kirim_seller AS biaya_kirim, transaksi.no_trx AS oreder_no, transaksi.buyer AS buyer, transaksi.tanggal_pemesanan AS tanggal_pemesanan, transaksi.alamat_pengiriman AS alamat FROM transaksi_seller JOIN transaksi ON transaksi_seller.id_transaksi = transaksi.id_transaksi JOIN status_transaksi ON transaksi.status_transaksi = status_transaksi.id WHERE transaksi_seller.id_toko =${id} AND transaksi.status_transaksi IN(4, 5) AND transaksi.complete = 1 ORDER BY transaksi.tanggal_pemesanan DESC`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getDetailTransaction = (idUser, idTransaksi) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT detail_transaksi.id_detail_transaksi AS id_transaksi, barang.harga_satuan AS harga, detail_transaksi.jumlah_pembelian AS jumlah, detail_transaksi.keterangan AS keterangan, detail_transaksi.nomor_resi AS nomor_resi, detail_transaksi.id_status AS status , detail_transaksi.tanggal_action AS tanggal_action, transaksi_seller.biaya_kirim_seller AS biaya_kirim, transaksi.no_trx AS oreder_no, transaksi.tanggal_pemesanan AS tanggal_pemesanan, transaksi.alamat_pengiriman AS alamat, barang.nama AS nama , barang.foto AS foto, barang.id AS id_barang FROM detail_transaksi JOIN transaksi_seller ON detail_transaksi.id_transaksi_seller = transaksi_seller.id JOIN barang ON detail_transaksi.id_barang = barang.id JOIN transaksi ON transaksi_seller.id_transaksi = transaksi.id_transaksi WHERE transaksi_seller.id_toko =${idUser} AND detail_transaksi.id_detail_transaksi = ${idTransaksi}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const tolakTransaksi = (id) => {
  return co(function * getByTambahQuery () {
    let sqlString = `UPDATE detail_transaksi SET id_status = 2, tanggal_action=now() WHERE id_detail_transaksi = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const prosesTransaksi = (id) => {
  return co(function * getByTambahQuery () {
    let sqlString = `UPDATE detail_transaksi SET id_status = 1, tanggal_action=now() WHERE id_detail_transaksi = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const updateResi = (id, nomor_resi) => {
  return co(function * getByTambahQuery () {
    let sqlString = `UPDATE detail_transaksi SET nomor_resi = '${nomor_resi}' WHERE id_detail_transaksi = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getDetailById = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM detail_transaksi WHERE id_detail_transaksi = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getTransaksiSellerById = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM transaksi_seller WHERE id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getTotalSaldo = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM pencairan_dana JOIN transaksi_seller ON pencairan_dana.id_transaksi_seller = transaksi_seller.id JOIN transaksi ON transaksi.id_transaksi = transaksi_seller.id_transaksi WHERE transaksi_seller.id_toko = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getMySaldo = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM pencairan_dana JOIN transaksi_seller ON pencairan_dana.id_transaksi_seller = transaksi_seller.id JOIN transaksi ON transaksi.id_transaksi = transaksi_seller.id_transaksi WHERE transaksi_seller.id_toko = ${id} AND pencairan_dana.status =0 ORDER BY pencairan_dana.waktu_request DESC`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getMyProsesSaldo = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM pencairan_dana JOIN transaksi_seller ON pencairan_dana.id_transaksi_seller = transaksi_seller.id JOIN transaksi ON transaksi.id_transaksi = transaksi_seller.id_transaksi WHERE transaksi_seller.id_toko = ${id} AND pencairan_dana.status =1 ORDER BY pencairan_dana.waktu_request DESC`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getHistorySaldo = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT * FROM pencairan_dana JOIN transaksi_seller ON pencairan_dana.id_transaksi_seller = transaksi_seller.id JOIN transaksi ON transaksi.id_transaksi = transaksi_seller.id_transaksi WHERE transaksi_seller.id_toko = ${id} AND pencairan_dana.status =2 ORDER BY pencairan_dana.waktu_request DESC`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}


const getUserByTransaksiSeller = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT toko.code_verifikasi AS kode FROM toko JOIN transaksi_seller ON toko.id = transaksi_seller.id_toko WHERE transaksi_seller.id IN(${id})`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const cairkanDana = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `UPDATE pencairan_dana SET status =1, waktu_request =now() WHERE id_transaksi_seller = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const cairkanSemuaDana = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `UPDATE pencairan_dana SET status =1, waktu_request =now() WHERE id_transaksi_seller IN (${id})`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getSaldoById = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT pencairan_dana.jumlah_uang AS jumlah_uang, pencairan_dana.bukti_transfer AS bukti_transfer, pencairan_dana.waktu_request AS waktu_request, pencairan_dana.waktu_transfer AS waktu_transfer, detail_transaksi.id_detail_transaksi AS id_transaksi, detail_transaksi.keterangan AS keterangan_barang, barang.harga_satuan AS harga, detail_transaksi.jumlah_pembelian AS jumlah, detail_transaksi.keterangan AS keterangan, detail_transaksi.nomor_resi AS nomor_resi, detail_transaksi.id_status AS status , detail_transaksi.tanggal_action AS tanggal_action, transaksi_seller.biaya_kirim_seller AS biaya_kirim, transaksi.keterangan AS keterangan_transaksi, transaksi.no_hp AS no_hp_buyer, transaksi.jasa_pengiriman AS jasa_pengiriman, transaksi.asuransi AS asuransi, transaksi.no_trx AS oreder_no, transaksi.tanggal_pemesanan AS tanggal_pemesanan, transaksi.alamat_pengiriman AS alamat, barang.nama AS nama, barang.foto AS foto, barang.id AS id_barang FROM detail_transaksi JOIN transaksi_seller ON detail_transaksi.id_transaksi_seller = transaksi_seller.id JOIN barang ON detail_transaksi.id_barang = barang.id JOIN transaksi ON transaksi_seller.id_transaksi = transaksi.id_transaksi JOIN pencairan_dana ON transaksi_seller.id = pencairan_dana.id_transaksi_seller WHERE transaksi_seller.id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}

const getTransactionBySellerId = (id) => {
  return co(function * getAllQuery () {
    let sqlString = `SELECT transaksi.status_transaksi FROM transaksi JOIN transaksi_seller ON transaksi.id_transaksi = transaksi_seller.id_transaksi WHERE transaksi_seller.id = ${id}`
    return new Promise ((resolve, reject) => {
      databaseConnection.query(sqlString, function(err, rows, fields) {
          if (!err){
            resolve(rows)
          }else{
            reject(err)
          }
      })
    })
  })
}



module.exports = {
  getAll,
  getAktif,
  getMyTransaction,
  getTheTransaction,
  getTheTransactionHistory,
  tolakTransaksi,
  updateResi,
  prosesTransaksi,
  getDetailTransaction,
  getDetailById,
  getTotalSaldo,
  getMySaldo,
  getMyProsesSaldo,
  getHistorySaldo,
  getSaldoById,
  getUserByTransaksiSeller,
  cairkanDana,
  cairkanSemuaDana,
  getTransactionBySellerId
}
