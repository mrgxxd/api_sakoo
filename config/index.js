'use strict'

const mysql = require('mysql');

const databaseConnection = mysql.createConnection({
  host     : 'localhost',
  port : '3306',
  user     : 'root',
  password : 'moklet',
  database : 'multiuser',
  dateStrings : true
});

const connection = () => {
  databaseConnection.connect(function(err){
    if(!err) {
        console.log("Database is connected ... nn")
    } else {
        console.log("Error connecting database ... nn")
    }
  })
}

const SECRET_KEY = "JackTheRipper"

module.exports = {
  databaseConnection,
  connection,
  SECRET_KEY
}
