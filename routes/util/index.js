'use strict'

const route = require('express').Router({mergeParams: true})
const md5 = require('md5')

const usersController = require('../../controller/user')
const barangController = require('../../controller/barang')
const transaksiController = require('../../controller/transaction')
const myController = require('../../controller/util')
const middlewere = require('../../middlewere')
const code = require('../../httpCode')

const handleError = code.handleError

route.get('/getVersion/:id', middlewere.isPublic, (req, res) => {

  let id = req.params.id
  myController.getVersion()
  .then(result => {
    usersController.getById(id)
    .then(getUser => {
      barangController.getByUserId(id)
      .then(getBarang => {
        transaksiController.getAktif(id)
        .then(getTransaksi => {
          transaksiController.getTotalSaldo(id)
          .then(getSaldo => {
            res.status(code.OK.code).send({code:200, message:"Success", data:{version : result[0], user : getUser[0], jumlahBarang : getBarang.length, jumlahTransaksi : getTransaksi.length, jumlahSaldo : getSaldo }})
          })
        })
      })
    })
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getKontak/:id', middlewere.isPublic, (req, res) => {

  let id = req.params.id

  myController.getKontak(id)
  .then(result => {
    res.status(code.OK.code).send({code:200, message:"Success", data:result[0]})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/dummyAPI', (req,res) => {
  res.status(code.OK.code).send({code:200, message:"Success"})
})

module.exports = route
