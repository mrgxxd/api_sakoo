'use strict'

const route = require('express').Router({mergeParams: true})
const md5 = require('md5')
const asyncLoop = require('node-async-loop');
const axios = require('axios');
var querystring = require('querystring');
var qs = require('qs');
const https = require('https');
var rp = require('request-promise');

const myController = require('../../controller/user')
const barangController = require('../../controller/barang')
const middlewere = require('../../middlewere')
const code = require('../../httpCode')

const handleError = code.handleError

const sendgrid = require('sendgrid')('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')

route.post('/login', middlewere.isPublic, (req,res) => {

  let username = req.body.username || ""
  let password = req.body.password || ""
  let regId = req.body.regId || ""
  let mode = req.body.mode || ""

  if(username != "" && password != "" && regId != "" && mode != ""){
    let md5Pass = md5(password)
    myController.login(username, md5Pass, mode)
    .then(getUser => {
      if(getUser.length == 1){
        middlewere.generateToken({id: getUser[0].id}, req.headers.secretkey)
        .then(getToken => {
          myController.updateRegToken(regId, getUser[0].id, getToken)
          .then(updateSuccess => {
            myController.getById(getUser[0].id)
            .then(result => {
              res.status(code.OK.code).send({code : 200, message: "Success", data:result[0], token : getToken})
            })
          })
        })
      }else{
        res.status(200).send({code : 404, message: "User tidak ditemukan, email / no hp dan password tidak cocok"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Mohon maaf terjadi kesalahan pada server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter tidak sesuai"})
  }
})

route.post('/newLogin', middlewere.isPublic, (req,res) => {

  let no_hp = req.body.no_hp || ""

  if(no_hp != ""){
    myController.getByPhone(no_hp)
    .then(getPhone => {
      if(getPhone.length == 1){
        let theBody = querystring.stringify({
          grant_type : 'client_credentials'
        })
        axios.post('https://api.mainapi.net/token', theBody, {
            headers: {
              'Authorization' : 'Basic cWZtbDlSdDRsQU1CWEVTc05KVDBSbXRBZER3YToya3BCR3JVWVNmSWRmNGdxNUNxcXJVRWRJU2th'
            }
        })
        .then(getCode => {
          let data = getCode.data
          let newCode = makecode()

          let newBody = querystring.stringify({
            msisdn : no_hp,
            content : "Kode verifikasi anda adalah "+newCode+". Jangan beritahukan kode ini pada orang lain. Terimakasih sudah menggunakan Sakoo.id"
          })
          myController.newLogin(no_hp, newCode)
          .then(result => {
            axios.post('https://api.mainapi.net/smsnotification/1.0.0/messages', newBody, {
                headers: {
                  'Accept' : 'application/json',
                  'Content-Type' : 'application/x-www-form-urlencoded',
                  'Authorization' : 'Bearer '+data.access_token
                }
            })
            .then(lastResult => {
              res.status(code.OK.code).send({code:200, message : "Success", data : result})
            })
          })
        })
      }else{
        res.status(code.OK.code).send({code:201, message : "Kontak tidak terdaftar, silahkan daftar terlebih dahulu"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.post('/codeSaldo', middlewere.isPublic, (req,res) => {

  let no_hp = req.body.no_hp || ""

  if(no_hp != ""){
    myController.getByPhone(no_hp)
    .then(getPhone => {
      if(getPhone.length == 1){
        let theBody = querystring.stringify({
          grant_type : 'client_credentials'
        })
        axios.post('https://api.mainapi.net/token', theBody, {
            headers: {
              'Authorization' : 'Basic cWZtbDlSdDRsQU1CWEVTc05KVDBSbXRBZER3YToya3BCR3JVWVNmSWRmNGdxNUNxcXJVRWRJU2th'
            }
        })
        .then(getCode => {
          let data = getCode.data
          let newCode = makecode()

          let newBody = querystring.stringify({
            msisdn : no_hp,
            content : "Kode pencairan dana anda adalah "+newCode+". Jangan beritahukan kode ini pada orang lain. Terimakasih sudah menggunakan Sakoo.id"
          })
          myController.newLogin(no_hp, newCode)
          .then(result => {
            axios.post('https://api.mainapi.net/smsnotification/1.0.0/messages', newBody, {
                headers: {
                  'Accept' : 'application/json',
                  'Content-Type' : 'application/x-www-form-urlencoded',
                  'Authorization' : 'Bearer '+data.access_token
                }
            })
            .then(lastResult => {
              res.status(code.OK.code).send({code:200, message : "Success", data : result})
            })
          })
        })
      }else{
        res.status(code.OK.code).send({code:201, message : "Kontak tidak terdaftar, silahkan daftar terlebih dahulu"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.post('/signin', middlewere.isPublic, (req,res) => {

  let no_hp = req.body.no_hp || ""

  if(no_hp != ""){
    myController.getByPhone(no_hp)
    .then(getPhone => {
      if(getPhone.length == 1){
        let theBody = querystring.stringify({
          grant_type : 'client_credentials'
        })
        axios.post('https://api.mainapi.net/token', theBody, {
            headers: {
              'Authorization' : 'Basic cWZtbDlSdDRsQU1CWEVTc05KVDBSbXRBZER3YToya3BCR3JVWVNmSWRmNGdxNUNxcXJVRWRJU2th'
            }
        })
        .then(getCode => {
          let data = getCode.data
          let newCode = makecode()

          let newBody = querystring.stringify({
            msisdn : no_hp,
            content : "Verifikasi code anda adalah "+newCode+". Terimakasih sudah menggunakan Sakoo.id"
          })
          myController.newLogin(no_hp, newCode)
          .then(result => {
            axios.post('https://api.mainapi.net/smsnotification/1.0.0/messages', newBody, {
                headers: {
                  'Accept' : 'application/json',
                  'Content-Type' : 'application/x-www-form-urlencoded',
                  'Authorization' : 'Bearer '+data.access_token
                }
            })
            .then(lastResult => {
              res.status(code.OK.code).send({code:200, message : "Success", data : result})
            })
          })
        })
      }else{
        res.status(code.OK.code).send({code:201, message : "Kontak tidak terdaftar, silahkan daftar terlebih dahulu"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.post('/register', middlewere.isPublic, (req,res) => {
  let username = req.body.username || ""
  let password = req.body.password || ""
  let email = req.body.email || ""
  let kontak = req.body.kontak || ""
  let nama_toko = req.body.nama_toko || ""

  if(username != "" && password != "" && email != "" && kontak != "" && nama_toko != ""){
    let md5Pass = md5(password)
    myController.getByPhone(kontak)
    .then(getUsername => {
      if(getUsername.length == 0){
        myController.getByEmail(email)
        .then(getEmail => {
          if(getEmail.length == 0){
            myController.register(username.replace("'","''"), md5Pass, email, kontak, nama_toko.replace("'","''"))
            .then(result => {
              res.status(code.OK.code).send({code:200, message : "Success", data : result})
            })
          }else{
            res.status(code.OK.code).send({code:201, message : "Email sudah digunakan"})
          }
        })
      }else{
        res.status(code.OK.code).send({code:201, message : "No handphone sudah digunakan"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.post('/newRegister', middlewere.isPublic, (req,res) => {
  let username = req.body.username || ""
  let nama_toko = req.body.nama_toko || ""
  let kontak = req.body.kontak || ""

  if(username != "" && kontak != "" && nama_toko != ""){
    myController.getByPhone(kontak)
    .then(getPhone => {
      if(getPhone.length == 0){
        let theBody = querystring.stringify({
          grant_type : 'client_credentials'
        })
        axios.post('https://api.mainapi.net/token', theBody, {
            headers: {
              'Authorization' : 'Basic cWZtbDlSdDRsQU1CWEVTc05KVDBSbXRBZER3YToya3BCR3JVWVNmSWRmNGdxNUNxcXJVRWRJU2th'
            }
        })
        .then(getCode => {
          let data = getCode.data
          let newCode = makecode()

          let newBody = querystring.stringify({
            msisdn : kontak,
            content : "Kode verifikasi anda adalah "+newCode+". Jangan beritahukan kode ini pada orang lain. Terimakasih sudah menggunakan Sakoo.id"
          })
          myController.newRegister(username, nama_toko, kontak, newCode)
          .then(result => {
            axios.post('https://api.mainapi.net/smsnotification/1.0.0/messages', newBody, {
                headers: {
                  'Accept' : 'application/json',
                  'Content-Type' : 'application/x-www-form-urlencoded',
                  'Authorization' : 'Bearer '+data.access_token
                }
            })
            .then(lastResult => {
              res.status(code.OK.code).send({code:200, message : "Success", data : result})
            })
          })
        })
      }else{
        res.status(code.OK.code).send({code:201, message : "Kontak sudah digunakan"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.post('/signup', middlewere.isPublic, (req,res) => {
  let username = req.body.username || ""
  let nama_toko = req.body.nama_toko || ""
  let kontak = req.body.kontak || ""

  if(username != "" && kontak != "" && nama_toko != ""){
    myController.getByPhone(kontak)
    .then(getPhone => {
      if(getPhone.length == 0){
        let theBody = querystring.stringify({
          grant_type : 'client_credentials'
        })
        axios.post('https://api.mainapi.net/token', theBody, {
            headers: {
              'Authorization' : 'Basic cWZtbDlSdDRsQU1CWEVTc05KVDBSbXRBZER3YToya3BCR3JVWVNmSWRmNGdxNUNxcXJVRWRJU2th'
            }
        })
        .then(getCode => {
          let data = getCode.data
          let newCode = makecode()

          let newBody = querystring.stringify({
            msisdn : kontak,
            content : "Verifikasi code anda adalah "+newCode+". Terimakasih sudah menggunakan Sakoo.id"
          })
          myController.newRegister(username, nama_toko, kontak, newCode)
          .then(result => {
            axios.post('https://api.mainapi.net/smsnotification/1.0.0/messages', newBody, {
                headers: {
                  'Accept' : 'application/json',
                  'Content-Type' : 'application/x-www-form-urlencoded',
                  'Authorization' : 'Bearer '+data.access_token
                }
            })
            .then(lastResult => {
              res.status(code.OK.code).send({code:200, message : "Success", data : result})
            })
          })
        })
      }else{
        res.status(code.OK.code).send({code:201, message : "Kontak sudah digunakan"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})


route.put('/updateProfile/:id', middlewere.isPublic, (req,res) => {
  let id = req.params.id

  let username = req.body.username || ""
  let email = req.body.email || ""
  let nama = req.body.nama || ""
  let no_hp = req.body.no_hp || ""
  let alamat = req.body.alamat || ""
  let foto = req.body.foto || ""
  let bank = req.body.bank || ""
  let rekening = req.body.rekening || ""
  let namarekening = req.body.namarekening || ""
  let lokasiMarkup = req.body.lokasi_markup || 1

  if(username != "" && email != "" && nama != "" && no_hp != "" && alamat != "" && bank != "" && rekening != "" && namarekening != ""){
    myController.getByEmail(email)
    .then(getEmail => {
      if(getEmail.length == 1 && getEmail[0].id == id){
        myController.getById(id)
        .then(userId => {
          myController.updateProfile(id, username.replace("'","''"), email, nama.replace("'","''"), no_hp, alamat.replace("'","''"), foto, bank, rekening, namarekening, lokasiMarkup)
          .then(updateSuccess => {
            myController.getById(id)
            .then(getIdforUser => {
              if(userId[0].lokasi_markup == lokasiMarkup){
                res.status(code.OK.code).send({code:200, message : "Success", data : getIdforUser[0]})
              }else{
                myController.getMarkUpById(lokasiMarkup)
                .then(getHargaMarkup => {
                  barangController.getByUserId(id)
                  .then(getMyBarang => {
                    if(getMyBarang.length != 0){
                      asyncLoop(getMyBarang, function(item, next){

                        let totalHarga = parseInt(item.harga) + parseInt(getHargaMarkup[0].markup)
                        let myProfit = Math.round(totalHarga * 0.05)
                        let tempHarga = totalHarga + myProfit + 100
                        let stringHarga = tempHarga.toString()
                        //let lastHarga = stringHarga.replace(stringHarga.substring(stringHarga.length -2),"00")
                        let lastHarga = totalHarga

                        barangController.editHargaMarkUp(id, lastHarga)
                        .then(afterMarkup => {
                          let idBarang = item.id
                          let deskripsiEdit = "Harga Markup,"
                          barangController.afterEditBarang(idBarang, deskripsiEdit)
                          .then(afterEdit => {
                            next()
                          })
                        })
                      }, function(){
                          res.status(code.OK.code).send({code:200, message : "Success", data : getIdforUser[0]})
                      })
                    }else{
                      res.status(code.OK.code).send({code:200, message : "Success", data : getIdforUser[0]})
                    }
                  })
                })
              }
            })
          })
        })
      }else if(getEmail.length == 0){
        myController.getById(id)
        .then(userId => {
          myController.updateProfile(id, username.replace("'","''"), email, nama.replace("'","''"), no_hp, alamat.replace("'","''"), foto, bank, rekening, namarekening, lokasiMarkup)
          .then(updateSuccess => {
            myController.getById(id)
            .then(getIdforUser => {
              if(userId[0].lokasi_markup == lokasiMarkup){
                res.status(code.OK.code).send({code:200, message : "Success", data : getIdforUser[0]})
              }else{
                myController.getMarkUpById(lokasiMarkup)
                .then(getHargaMarkup => {
                  barangController.getByUserId(id)
                  .then(getMyBarang => {
                    if(getMyBarang.length != 0){
                      asyncLoop(getMyBarang, function(item, next){
                        barangController.editHargaMarkUp(id, getHargaMarkup[0].markup)
                        .then(afterMarkup => {
                          let idBarang = item.id
                          let deskripsiEdit = "Harga Markup,"
                          barangController.afterEditBarang(idBarang, deskripsiEdit)
                          .then(afterEdit => {
                            next()
                          })
                        })
                      }, function(){
                          res.status(code.OK.code).send({code:200, message : "Success", data : getIdforUser[0]})
                      })
                    }else{
                      res.status(code.OK.code).send({code:200, message : "Success", data : getIdforUser[0]})
                    }
                  })
                })
              }
            })
          })
        })
      }else{
        res.status(code.OK.code).send({code:201, message : "Email sudah digunakan"})
      }
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.put('/gantiPassword', middlewere.isLogin, (req,res) => {

  let id = req.decoded.mytoken

  let passwordBaru = req.body.passwordBaru || ""
  let md5Baru = md5(passwordBaru)

  myController.getById(id)
  .then(getUser => {
    if(getUser.length == 1){
      myController.updatePassword(id, md5Baru)
      .then(result => {
        res.status(code.OK.code).send({code:200, message : "Password berhasil diganti", data : result[0]})
      })
    }else{
      res.status(code.OK.code).send({code:404, message : "User tidak ditemukan"})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.post('/newLupapassword', (req,res) => {
  let no_hp = req.body.no_hp

  myController.getByPhone(no_hp)
  .then(getPhone => {
    if(getPhone.length == 1){
      middlewere.generateToken({mytoken : getPhone[0].id}, "JackTheRipper")
      .then(myToken => {
        console.log(myToken)
        let theBody = querystring.stringify({
          grant_type : 'client_credentials'
        })
        axios.post('https://api.mainapi.net/token', theBody, {
            headers: {
              'Authorization' : 'Basic cWZtbDlSdDRsQU1CWEVTc05KVDBSbXRBZER3YToya3BCR3JVWVNmSWRmNGdxNUNxcXJVRWRJU2th'
            }
        })
        .then(getCode => {
          let data = getCode.data

          let newBody = querystring.stringify({
            msisdn : no_hp,
            content : "Silahkan klik http://sakoo.id/changePassword?token="+myToken+" untuk mengubah passsword akun sakoo anda. Link ini hanya berlaku 1 jam. Terimakasih sudah menggunakan Sakoo.id"
          })
          axios.post('https://api.mainapi.net/smsnotification/1.0.0/messages', newBody, {
              headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/x-www-form-urlencoded',
                'Authorization' : 'Bearer '+data.access_token
              }
          })
          .then(lastResult => {
            res.status(code.OK.code).send({code:200, message : "Success"})
          })
        })
      })
    }else{
      res.status(code.OK.code).send({code:404, message : "No handphone tidak terdaftar"})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.post('/lupapassword', (req,res) => {

  let email = req.body.email

  myController.getByEmail(email)
  .then(result => {
    if(result.length == 1){
      let newPassword = makeid()
      console.log(newPassword)
      let md5Pass = md5(newPassword)
      myController.updatePassword(result[0].id, md5Pass)
      .then(hasil => {
        console.log(hasil)
        var request = sendgrid.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [
              {
                to: [
                  {
                    email: email,
                  },
                ],
                subject: 'Permintaan ganti password',
              },
            ],
            from: {
              email: 'no-reply@sakoo.id',
            },
            content: [
              {
                type: 'text/html',
                value: '<html lang="en">'+
                        '<body>'+
                          '<div bgcolor="#ffffff" style="margin:0; padding:0; font-family:sans-serif!important; height:100%; color:#484848!important; width:100%!important; background-color:#ffffff; line-height:1.6;">'+
                            '<table style="margin:0; padding:0; margin-top:2em; font-family:sans-serif!important; height:100%; color:#222; background-color:#ffffff; width:100%; line-height:1.6;">'+
                              '<tr style="margin:0; padding:0;">'+
                                '<td style="padding:0; display:block; margin:0 auto!important; clear:both!important; max-width:610px!important;">'+
                                  '<div style="max-width:450px; margin:30px auto; display:block; background-color:#ffffff;">'+
                                    '<div style="margin:0; padding:0; margin-bottom:1em; display:block; color:#484848!important;">Username anda adalah '+result[0].username+', dan kami telah menyetel ulang kata sandi anda menjadi '+newPassword+', Terima Kasih.</div>'+
                                  '</div>'+
                                '</td>'+
                              '</tr>'+
                            '</table>'+
                          '</div>'+
                        '</body>'+
                      '</html>'
                //'silahkan ' + str.link('https://web.selena.id/verification/'+created.id) + ' untuk mengaktifkan akun anda',

              },
            ],
          }
        })

        sendgrid.API(request)
          .then(response => { res.send({code : 200, data: response}) })
          .catch(error => { res.send({code : 404, data: error}) })
      })
    }else{
      res.status(200).send({code : 404, message: "Data tidak ditemukan"})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getBank', (req,res) => {
  myController.getBank()
  .then(result => {
    myController.getMarkUp()
    .then(getMarkup => {
      res.status(code.OK.code).send({code: 200, message: "Success", data:result, dataMarkup:getMarkup})
    })
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getMarkUp', (req,res) => {
  myController.getMarkUp()
  .then(result => {
    res.status(code.OK.code).send({code: 200, message: "Success", data:result})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.post('/newCode', (req,res) => {

  let email = req.body.email || 'bagus.freelancer@gmail.com'

  let theBody = querystring.stringify({
    grant_type : 'client_credentials'
  })
  axios.post('https://api.mainapi.net/token', theBody, {
      headers: {
        'Authorization' : 'Basic Vk5oQmxoQ2NfcmNNTXJ1R2x2bTE1X0FuaTZFYTpGbTBYWnIwX1NhWDBFYUpNT0doTGR5OWFzZWNh'
      }
  })
  .then(result => {
    let data = result.data
    myController.getByEmail(email)
    .then(getEmail => {
      let no_hp = getEmail[0].no_hp
      let newCode = makecode()

      let newBody = querystring.stringify({
        msisdn : no_hp,
        content : "Verifikasi code anda adalah "+newCode+". Jangan beritahukan kode ini kepada orang lain. Terimakasih sudah menggunakan Sakoo.id"
      })
      axios.post('https://api.mainapi.net/smsnotification/1.0.0/messages', newBody, {
          headers: {
            'Accept' : 'application/json',
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Authorization' : 'Bearer '+data.access_token
          }
      })
      .then(lastResult => {
        res.status(code.OK.code).send({code: 200, message: "Success"})
      })
    })
  })
  .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.post('/verifikasiCode', (req,res) => {

  let kontak = req.body.no_hp || 'bagus.freelancer@gmail.com'
  let myCode = req.body.myCode || '4567'
  let regId = req.body.regId || ""

  myController.getByPhone(kontak)
  .then(getPhone => {
    if(getPhone.length == 1){
      if(getPhone[0].code_verifikasi == myCode){
        middlewere.generateToken({id: getPhone[0].id}, req.headers.secretkey)
        .then(getToken => {
          myController.updateRegToken(regId, getPhone[0].id, getToken)
          .then(updateSuccess => {
            myController.getById(getPhone[0].id)
            .then(result => {
              res.status(code.OK.code).send({code : 200, message: "Success", data:result[0], token : getToken})
            })
          })
        })
      }else{
        res.status(code.OK.code).send({code: 201, message: "Kode yang anda masukan salah"})
      }
    }else{
      res.status(code.OK.code).send({code: 404, message: "Kontak tidak ditemukan"})
    }
  })
  .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.post('/verification', (req,res) => {

  let kontak = req.body.no_hp || 'bagus.freelancer@gmail.com'
  let myCode = req.body.verification_code || '4567'
  let regId = req.body.reg_id || ""

  myController.getByPhone(kontak)
  .then(getPhone => {
    if(getPhone.length == 1){
      if(getPhone[0].code_verifikasi == myCode){
        middlewere.generateToken({id: getPhone[0].id}, req.headers.secretkey)
        .then(getToken => {
          myController.updateRegToken(regId, getPhone[0].id, getToken)
          .then(updateSuccess => {
            myController.getById(getPhone[0].id)
            .then(result => {
              result[0].token = getToken
              res.status(code.OK.code).send({message: "Login Berhasil", data:result[0]})
            })
          })
        })
      }else{
        res.status(code.OK.code).send({message: "Kode yang anda masukan salah"})
      }
    }else{
      res.status(200).send({code : 404, message: "No Hp tidak ditemukan"})
    }
  })
  .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/makeCode', (req,res) => {
  let newCode = makecode()
  res.send(newCode)
})

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function makecode() {
  var text = "";
  var possible = "0123456789";

  for (var i = 0; i < 4; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


module.exports = route
