'use strict'

const route = require('express').Router({mergeParams: true})

const myController = require('../../controller/subcategory')
const middlewere = require('../../middlewere')
const code = require('../../httpCode')

const handleError = code.handleError

route.get('/getSubCategory', (req,res) => {
  myController.getAll()
  .then(result => {
    res.status(code.OK.code).send(result)
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getSubByCategory/:id', (req,res) => {

  let id = req.params.id

  if(id != ""){
    myController.getByCategory(id)
    .then(result => {
      res.status(code.OK.code).send({code: 200, message: "Success", data:result})
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

module.exports = route
