'use strict'

const route = require('express').Router({mergeParams: true})

const myController = require('../../controller/category')
const middlewere = require('../../middlewere')
const code = require('../../httpCode')

const handleError = code.handleError

route.get('/getCategory', (req,res) => {
  myController.getAll()
  .then(result => {
    res.status(code.OK.code).send({code: 200, message: "Success", data:result})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

module.exports = route
