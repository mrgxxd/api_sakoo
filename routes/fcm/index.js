'use strict'

const route = require('express').Router({mergeParams: true})

const myController = require('../../controller/fcm')
const userController = require('../../controller/user')
const barangController = require('../../controller/barang')
const transactionController = require('../../controller/transaction')
const middlewere = require('../../middlewere')
const code = require('../../httpCode')

const handleError = code.handleError

route.post('/sendOneNotification', (req,res) => {

  let id = req.body.userId || ""
  let title = req.body.title || ""
  let body = req.body.body || ""

  let type = req.body.type || ""
  let typeId = req.body.id || ""

  let paramNotif = req.body.paramNotif || ""

  userController.getById(id)
  .then(getId => {
    if(getId.length == 1){
      let token = getId[0].reg_id
      myController.insertNotificationUser(title, body, typeId, type, id)
      .then(insertSuccess => {
        if(paramNotif == "kirim"){
          let status_proses = ""
          if(type == "transaksi"){
            transactionController.getTransactionBySellerId(id)
            .then(getTheTransaction => {
              if(getTheTransaction[0].status_transaksi == 1 ){
                status_proses = "Perlu Proses"
              }else if(getTheTransaction[0].status_transaksi == 2){
                status_proses = "Input Resi"
              }else if(getTheTransaction[0].status_transaksi == 3){
                status_proses = "Pengiriman"
              }else if(getTheTransaction[0].status_transaksi == 4){
                status_proses = "Selesai"
              }
              myController.getByTypeId(typeId)
              .then(getType => {
                if(getType.length > 0){
                  myController.updateNotificationUserHome(title, body, typeId, type, id, status_proses)
                  .then(insertHomeSuccess => {
                    myController.sendOneNotification(token, title, body, typeId, type)
                    .then(result => {
                      res.status(code.OK.code).send({code: 200, message: "Success", data:result})
                    })
                  })
                }else{
                  myController.insertNotificationUserHome(title, body, typeId, type, id, status_proses)
                  .then(insertHomeSuccess => {
                    myController.sendOneNotification(token, title, body, typeId, type)
                    .then(result => {
                      res.status(code.OK.code).send({code: 200, message: "Success", data:result})
                    })
                  })
                }
              })
            })
          }else if(type == "barang"){
            barangController.getUploadBarangById(typeId)
            .then(getBarang => {
              for(var i = 0; i < getBarang.length; i++){
                if(getBarang[i].status_ecommerce == 3){
                  status_proses = "Sukses"
                  i = getBarang.length
                }else{
                  if(getBarang[0].status_penyebab_unlisting == 1){
                    status_proses = "Proses"
                  }else if(getBarang[0].status_penyebab_unlisting == 2 || getBarang[0].status_penyebab_unlisting == 3){
                    status_proses = "Tolak"
                  }else if(getBarang[0].status_penyebab_unlisting == 4) {
                    status_proses = "Perlu Proses"
                  }
                }
              }
              myController.getByTypeId(typeId)
              .then(getType => {
                if(getType.length > 0){
                  myController.updateNotificationUserHome(title, body, typeId, type, id, status_proses)
                  .then(insertHomeSuccess => {
                    myController.sendOneNotification(token, title, body, typeId, type)
                    .then(result => {
                      res.status(code.OK.code).send({code: 200, message: "Success", data:result})
                    })
                  })
                }else{
                  myController.insertNotificationUserHome(title, body, typeId, type, id, status_proses)
                  .then(insertHomeSuccess => {
                    myController.sendOneNotification(token, title, body, typeId, type)
                    .then(result => {
                      res.status(code.OK.code).send({code: 200, message: "Success", data:result})
                    })
                  })
                }
              })
            })
          }else if(type == "saldo"){
            status_proses = "Selesai"
            myController.getByTypeId(typeId)
            .then(getType => {
              if(getType.length > 0){
                myController.updateNotificationUserHome(title, body, typeId, type, id, status_proses)
                .then(insertHomeSuccess => {
                  myController.sendOneNotification(token, title, body, typeId, type)
                  .then(result => {
                    res.status(code.OK.code).send({code: 200, message: "Success", data:result})
                  })
                })
              }else{
                myController.insertNotificationUserHome(title, body, typeId, type, id, status_proses)
                .then(insertHomeSuccess => {
                  myController.sendOneNotification(token, title, body, typeId, type)
                  .then(result => {
                    res.status(code.OK.code).send({code: 200, message: "Success", data:result})
                  })
                })
              }
            })
          }else{
            myController.sendOneNotification(token, title, body, typeId, type)
            .then(result => {
              res.status(code.OK.code).send({code: 200, message: "Success", data:result})
            })
          }
        }else{
          myController.sendOneNotification(token, title, body, typeId, type)
          .then(result => {
            res.status(code.OK.code).send({code: 200, message: "Success", data:result})
          })
        }
      })
    }else {
      res.status(200).send({code : 404, message: "User tidak ditemukan"})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getNotification/:id', (req,res) => {
  let id = req.params.id

  myController.getNotificationById(id)
  .then(result => {
    res.status(code.OK.code).send({code: 200, message: "Success", data:result})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getHomeNotif/:id', (req,res) => {
  let id = req.params.id

  myController.getHomeNotifById(id)
  .then(result => {
    barangController.getByUserId(id)
    .then(getBarang => {
      transactionController.getAktif(id)
      .then(getTransaksi => {
        transactionController.getTotalSaldo(id)
        .then(getSaldo => {
          res.status(code.OK.code).send({code: 200, message: "Success", data:result , dataTambahan:{jumlahBarang : getBarang.length, jumlahTransaksi : getTransaksi.length, jumlahSaldo : getSaldo }})
        })
      })
    })
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

module.exports = route
