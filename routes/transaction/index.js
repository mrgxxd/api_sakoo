'use strict'

const route = require('express').Router({mergeParams: true})
const md5 = require('md5')

const myController = require('../../controller/transaction')
const barangController = require('../../controller/barang')
const userController = require('../../controller/user')
const fcmController = require('../../controller/fcm')
const middlewere = require('../../middlewere')
const code = require('../../httpCode')

const handleError = code.handleError

route.get('/getMyTransaction/:id', middlewere.isPublic, (req, res) => {

  let id = req.params.id || ""

  if(id != ""){
    myController.getMyTransaction(id)
    .then(result => {
      res.status(code.OK.code).send({code:200, message:"Success", data:result})
    })
    .catch(response => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.get('/getTheTransaction/:id', middlewere.isPublic, (req, res) => {

  let id = req.params.id || ""

  if(id != ""){
    myController.getTheTransaction(id)
    .then(result => {
      res.status(code.OK.code).send({code:200, message:"Success", data:result})
    })
    .catch(response => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.get('/getTheTransactionHistory/:id', middlewere.isPublic, (req, res) => {

  let id = req.params.id || ""

  if(id != ""){
    myController.getTheTransactionHistory(id)
    .then(result => {
      res.status(code.OK.code).send({code:200, message:"Success", data:result})
    })
    .catch(response => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.post('/getDetailTransaction', middlewere.isPublic, (req, res) => {

  let idUser = req.body.idUser || ""
  let idTransaksi = req.body.idTransaksi || ""

  if(idUser != "" && idTransaksi != ""){
    myController.getDetailTransaction(idUser, idTransaksi)
    .then(result => {
      if(result.length == 1){
        res.status(code.OK.code).send({code:200, message:"Success", data:result[0]})
      }else{
        res.status(200).send({code : 404, message: "Data tidak ditemukan"})
      }
    })
    .catch(response => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.put('/tolakTransaksi/:id', middlewere.isPublic, (req,res) => {
  let id = req.params.id

  myController.tolakTransaksi(id)
  .then(result => {
    myController.getDetailById(id)
    .then(getId => {
      let idBarang = getId[0].id_barang
      let jumlah = getId[0].jumlah_pembelian
      barangController.getById(idBarang)
      .then(getBarang => {
        let stok = getBarang[0].stok
        let newStok = stok + jumlah
        barangController.addStockAfterDelete(idBarang, newStok)
        .then(updateStok => {
          let newStatus = 5
          barangController.updateEcommerceAfterAddStock(idBarang, newStatus)
          .then(hasilAkhir => {
            res.status(code.OK.code).send({code:200, message:"Success"})
          })
        })
      })
    })
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.put('/prosesTransaksi/:id', middlewere.isPublic, (req,res) => {
  let id = req.params.id

  myController.prosesTransaksi(id)
  .then(result => {
    res.status(code.OK.code).send({code:200, message:"Success"})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.put('/updateResi/:id', middlewere.isPublic, (req,res) => {
  let id = req.params.id

  let nomor_resi = req.body.nomor_resi

  myController.updateResi(id, nomor_resi)
  .then(result => {
    res.status(code.OK.code).send({code:200, message:"Success"})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getSaldo/:id', middlewere.isPublic, (req,res) => {
  let id = req.params.id

  myController.getMySaldo(id)
  .then(result => {
    res.status(code.OK.code).send({code:200, message:"Success", data:result})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getMyProsesSaldo/:id', middlewere.isPublic, (req,res) => {
  let id = req.params.id

  myController.getMyProsesSaldo(id)
  .then(result => {
    res.status(code.OK.code).send({code:200, message:"Success", data:result})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getHistorySaldo/:id', (req,res) => {

  let id = req.params.id

  myController.getHistorySaldo(id)
  .then(result => {
    res.status(code.OK.code).send({code:200, message:"Success", data:result})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.put('/cairkanDana/:id', middlewere.isPublic, (req,res) => {
  let id = req.params.id
  let myCode = req.body.code_verifikasi

  myController.getUserByTransaksiSeller(id)
  .then(getUser => {
    if(getUser.length == 1){
      if(getUser[0].kode == myCode){
        myController.cairkanDana(id)
        .then(result => {
          res.status(code.OK.code).send({code:200, message:"Success"})
        })
      }else{
        res.status(code.OK.code).send({code:201, message:"Kode tidak sesuai"})
      }
    }else{
      res.status(code.OK.code).send({code:404, message:"Data Tidak ditemukan"})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.post('/cairkanSemuaDana', middlewere.isPublic, (req,res) => {
  let id = req.body.id
  let myCode = req.body.code_verifikasi

  myController.getUserByTransaksiSeller(id)
  .then(getUser => {
    if(getUser.length > 0){
      if(getUser[0].kode == myCode){
        myController.cairkanSemuaDana(id)
        .then(result => {
          res.status(code.OK.code).send({code:200, message:"Success"})
        })
      }else{
        res.status(code.OK.code).send({code:201, message:"Kode tidak sesuai"})
      }
    }else{
      res.status(code.OK.code).send({code:404, message:"Data tidak ditemukan"})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.get('/getDetailSaldo/:id', middlewere.isPublic, (req,res) => {

  let id = req.params.id

  myController.getSaldoById(id)
  .then(result => {
    res.status(code.OK.code).send({code:200, message:"Success", data:result})
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })

})

module.exports = route
