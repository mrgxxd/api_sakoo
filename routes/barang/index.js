'use strict'

const route = require('express').Router({mergeParams: true})
const asyncLoop = require('node-async-loop');

const myController = require('../../controller/barang')
const userController = require('../../controller/user')
const fcmController = require('../../controller/fcm')
const middlewere = require('../../middlewere')
const code = require('../../httpCode')

const handleError = code.handleError

route.get('/', middlewere.isLogin, (req,res) => {

  let userId = req.decoded.id

  let status = req.query.status || ""
  let searching = req.body.searching || ""
  let sorting = req.body.sorting || ""
  let index = req.body.index || 0

  myController.getBarangByUser(userId, searching, status, sorting, index)
  .then(result => {
    if(result.length != 0){
      let i = 0
      asyncLoop(result, function(item, next){
        let id = item.id_barang
        myController.getEcommerce(id)
        .then(getEcommerce => {
          var ecommerce = []
          var status = "unlisting"
          for(var j = 0; j<getEcommerce.length; j++){
            var tempEcommerce = {}
            tempEcommerce.nama_ecommerce = getEcommerce[j].nama_ecommerce
            tempEcommerce.url = getEcommerce[j].url
            tempEcommerce.status = getEcommerce[j].status

            if(getEcommerce[j].status == 3 && result[i].stok > 0){
              status = "listing"
            }
            ecommerce.push(tempEcommerce)
          }
          result[i].ecommerce = ecommerce
          result[i].status = status

          myController.getFotoTambahan(id)
          .then(getFoto => {
            result[i].foto_tambahan = getFoto
            i++
            next()
          })
        })
      }, function(){
        res.status(200).send({code : 200, message: "Success", data:result})
      })
    }else{
      res.status(200).send({code : 200, message: "Success", data:result})
    }
  })
  .catch(reason => {
    res.send(reason)
  })
})

route.post('/tambahBarang', middlewere.isPublic, (req,res) => {

  let id = req.body.id || ""
  let foto1 = req.body.foto1 || ""
  let foto2 = req.body.foto2 || ""
  let foto3 = req.body.foto3 || ""
  let foto4 = req.body.foto4 || ""
  let foto5 = req.body.foto5 || ""
  let nama = req.body.nama || ""
  let subkategori = req.body.subkategori || ""
  let deskripsi = req.body.deskripsi || ""
  let harga = req.body.harga || ""
  let stok = req.body.stok || ""
  let berat = req.body.berat || ""
  let volume = req.body.volume || ""
  let bahan = req.body.bahan || ""
  let merk = req.body.merk || ""
  let statusBarang = req.body.statusBarang || 1

  if(id != "" && foto1 != "" && nama != "" && subkategori != "" && deskripsi != "" && harga != "" && harga != "" && stok != "" && berat != "" && statusBarang != ""){
    myController.getAllBarang()
    .then(maxId => {
      let unikId = maxId[0].max + 1
      let newNama = nama + " sk-" + unikId
      userController.getUserMarkUp(id)
      .then(getUserMarkUp => {

        let hargaMarkup = getUserMarkUp[0].markup
        let totalHarga = parseInt(harga) + parseInt(hargaMarkup)
        let myProfit = Math.round(totalHarga * 0.05)
        let tempHarga = totalHarga + myProfit + 100
        let stringHarga = tempHarga.toString()
        //let lastHarga = stringHarga.replace(stringHarga.substring(stringHarga.length -2),"00")
        let lastHarga = totalHarga

        myController.tambahBarang(id, subkategori, foto1, newNama.replace("'","''"), deskripsi.replace("'","''"), harga, stok, berat, lastHarga, volume, bahan.replace("'","''"), merk.replace("'","''"), statusBarang)
        .then(created => {
          myController.getByTambah(subkategori, newNama.replace("'","''"), foto1, deskripsi.replace("'","''"), harga, stok)
          .then(getTambah => {
            fcmController.insertNotificationUserHome("Upload barang", "Barang "+nama.replace("'","''")+" sedang dalam pengecekan oleh admin", getTambah[0].id, "barang", id, "Proses")
            .then(sendNotif => {
              if(foto2 != ""){
                myController.tambahFoto(getTambah[0].id, foto2)
                .then(tambahFoto2 => {
                  if(foto3 != ""){
                    myController.tambahFoto(getTambah[0].id, foto3)
                    .then(tambahFoto3 => {
                      if(foto4 != ""){
                        myController.tambahFoto(getTambah[0].id, foto4)
                        .then(tambahFoto4 => {
                          if(foto5 != ""){
                            myController.tambahFoto(getTambah[0].id, foto5)
                            .then(tambahFoto5 => {
                              myController.tambahUploadBarang(getTambah[0].id, '1')
                              .then(tambahUploadBarang1 => {
                                myController.tambahUploadBarang(getTambah[0].id, '2')
                                .then(tambahUploadBarang2 => {
                                  myController.tambahUploadBarang(getTambah[0].id, '3')
                                  .then(tambahUploadBarang3 => {
                                    myController.tambahUploadBarang(getTambah[0].id, '4')
                                    .then(tambahUploadBarang4 => {
                                      res.status(200).send({code : 200, message: "Success"})
                                    })
                                  })
                                })
                              })
                            })
                          }else {
                            myController.tambahUploadBarang(getTambah[0].id, '1')
                            .then(tambahUploadBarang1 => {
                              myController.tambahUploadBarang(getTambah[0].id, '2')
                              .then(tambahUploadBarang2 => {
                                myController.tambahUploadBarang(getTambah[0].id, '3')
                                .then(tambahUploadBarang3 => {
                                  myController.tambahUploadBarang(getTambah[0].id, '4')
                                  .then(tambahUploadBarang4 => {
                                    res.status(200).send({code : 200, message: "Success"})
                                  })
                                })
                              })
                            })
                          }
                        })
                      }else{
                        myController.tambahUploadBarang(getTambah[0].id, '1')
                        .then(tambahUploadBarang1 => {
                          myController.tambahUploadBarang(getTambah[0].id, '2')
                          .then(tambahUploadBarang2 => {
                            myController.tambahUploadBarang(getTambah[0].id, '3')
                            .then(tambahUploadBarang3 => {
                              myController.tambahUploadBarang(getTambah[0].id, '4')
                              .then(tambahUploadBarang4 => {
                                res.status(200).send({code : 200, message: "Success"})
                              })
                            })
                          })
                        })
                      }
                    })
                  }else{
                    myController.tambahUploadBarang(getTambah[0].id, '1')
                    .then(tambahUploadBarang1 => {
                      myController.tambahUploadBarang(getTambah[0].id, '2')
                      .then(tambahUploadBarang2 => {
                        myController.tambahUploadBarang(getTambah[0].id, '3')
                        .then(tambahUploadBarang3 => {
                          myController.tambahUploadBarang(getTambah[0].id, '4')
                          .then(tambahUploadBarang4 => {
                            res.status(200).send({code : 200, message: "Success"})
                          })
                        })
                      })
                    })
                  }
                })
              }else{
                myController.tambahUploadBarang(getTambah[0].id, '1')
                .then(tambahUploadBarang1 => {
                  myController.tambahUploadBarang(getTambah[0].id, '2')
                  .then(tambahUploadBarang2 => {
                    myController.tambahUploadBarang(getTambah[0].id, '3')
                    .then(tambahUploadBarang3 => {
                      myController.tambahUploadBarang(getTambah[0].id, '4')
                      .then(tambahUploadBarang4 => {
                        res.status(200).send({code : 200, message: "Success"})
                      })
                    })
                  })
                })
              }
            })
          })
        })
      })
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan mohon periksa kembali inputan anda"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.put('/editBarang/:id', middlewere.isPublic, (req,res) => {

  let id = req.params.id || ""
  let foto1 = req.body.foto1 || ""
  let foto2 = req.body.foto2 || ""
  let foto3 = req.body.foto3 || ""
  let foto4 = req.body.foto4 || ""
  let foto5 = req.body.foto5 || ""
  let nama = req.body.nama || ""
  let subkategori = req.body.subkategori || ""
  let deskripsi = req.body.deskripsi || ""
  let harga = req.body.harga || ""
  let stok = req.body.stok || ""
  let berat = req.body.berat || ""
  let volume = req.body.volume || ""
  let bahan = req.body.bahan || ""
  let merk = req.body.merk || ""
  let deskripsiEdit = ""

  if(id != "" && foto1 != "" && nama != "" && subkategori != "" && deskripsi != "" && harga != "" && harga != "" && stok != "" && berat != ""){
    let newNama = nama + " sk-" + id
    myController.getById(id)
    .then(getBarang => {
      let idToko = getBarang[0].id_toko
      userController.getUserMarkUp(idToko)
      .then(getUserMarkUp => {
        let hargaMarkup = getUserMarkUp[0].markup
        let totalHarga = parseInt(harga) + parseInt(hargaMarkup)
        let myProfit = Math.round(totalHarga * 0.05)
        let tempHarga = totalHarga + myProfit + 100
        let stringHarga = tempHarga.toString()
        //let lastHarga = stringHarga.replace(stringHarga.substring(stringHarga.length -2),"00")
        let lastHarga = totalHarga

        if(getBarang[0].nama != newNama){
          deskripsiEdit = deskripsiEdit + "Nama Barang,"
        }

        if(getBarang[0].id_subkategori != subkategori){
          deskripsiEdit = deskripsiEdit + "Kategori Barang,"
        }

        if(getBarang[0].deskripsi.trim() != deskripsi){
          deskripsiEdit = deskripsiEdit + "Deskripsi Barang,"
        }

        if(getBarang[0].harga_satuan != harga){
          deskripsiEdit = deskripsiEdit + "Harga, Harga MarkUp,"
        }

        if(getBarang[0].stok != stok){
          deskripsiEdit = deskripsiEdit + "Stok,"
        }

        if(getBarang[0].berat != berat){
          deskripsiEdit = deskripsiEdit + "Berat,"
        }

        if(getBarang[0].volume != volume){
          deskripsiEdit = deskripsiEdit + "Volume,"
        }

        if(getBarang[0].bahan != bahan){
          deskripsiEdit = deskripsiEdit + "Bahan,"
        }

        if(getBarang[0].merk != merk){
          deskripsiEdit = deskripsiEdit + "Merk,"
        }

        if(getBarang[0].foto != foto1){
          deskripsiEdit = deskripsiEdit + "Foto,"
        }

        myController.editBarang(id, subkategori, foto1, newNama.replace("'","''"), deskripsi.replace("'","''"), harga, stok, berat, lastHarga, volume, bahan.replace("'","''"), merk.replace("'","''"))
        .then(created => {
          // myController.updateBarangAfterEdit(id)
          // .then(updateBarang => {
            myController.afterEditBarang(id,deskripsiEdit)
            .then(afterEditBarang => {
              myController.hapusFoto(id)
              .then(getTambah => {
                if(foto2 != ""){
                  myController.tambahFoto(getBarang[0].id, foto2)
                  .then(tambahFoto2 => {
                    if(foto3 != ""){
                      myController.tambahFoto(getBarang[0].id, foto3)
                      .then(tambahFoto3 => {
                        if(foto4 != ""){
                          myController.tambahFoto(getBarang[0].id, foto4)
                          .then(tambahFoto4 => {
                            if(foto5 != ""){
                              myController.tambahFoto(getBarang[0].id, foto5)
                              .then(tambahFoto5 => {
                                  res.status(200).send({code : 200, message: "Success"})
                              })
                            }else {
                                res.status(200).send({code : 200, message: "Success"})
                            }
                          })
                        }else{
                            res.status(200).send({code : 200, message: "Success"})
                        }
                      })
                    }else{
                        res.status(200).send({code : 200, message: "Success"})
                    }
                  })
                }else{
                    res.status(200).send({code : 200, message: "Success"})
                }
              })
            })
          //})
        })
      })
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan mohon periksa kembali inputan anda"})
    })
  }else{
    res.status(200).send({code : 403, message: "Parameter yang disertakan tidak sesuai"})
  }
})

route.get('/getListing/:id', middlewere.isPublic, (req,res) => {
  let userId = req.params.id
  let index = req.query.index

  myController.getListing(userId)
  .then(result => {
    if(result.length > 1){
      myController.disticntBarang(result)
      .then(hasilDistinct => {
        let lengthHasil = hasilDistinct.length //20
        if(index < lengthHasil){
          let batasAkhir = parseInt(index) + 10 // 15 , 20
          let batasArray = ""
          if(batasAkhir >= lengthHasil){
            batasArray = lengthHasil
          }else{
            batasArray = batasAkhir
          }
          let newHasil = hasilDistinct.slice(index, batasArray)
          let i = 0
          let ecommerce = []
          let urlEcommerce = []
          asyncLoop(newHasil, function(item, next){
            asyncLoop(result, function(item2, next2){
              if(item.id_barang == item2.id_barang){
                ecommerce.push(item2.nama_ecommerce)
                urlEcommerce.push(item2.url)
              }
              next2()
            }, function(){
              let uniqueEcommerce = ecommerce.filter(function(elem, index, self) {
                  return index == self.indexOf(elem);
              })
              let uniqueUrl = urlEcommerce.filter(function(elem, index, self) {
                  return index == self.indexOf(elem);
              })
              newHasil[i].nama_ecommerce = uniqueEcommerce
              newHasil[i].url = uniqueUrl
              i++
              ecommerce = []
              urlEcommerce = []
              next()
            })
          }, function(){
            res.status(200).send({code : 200, message: "Success", total:lengthHasil, data:newHasil})
          })
        }else{
          res.status(200).send({code : 200, message: "Success", total:lengthHasil, data:[]})
        }
      })
    }else{
      res.status(200).send({code : 200, message: "Success", total:result.length, data:result})
    }
  })
  .catch(reason => {
    res.send(reason)
  })
})

route.get('/getUnListing/:id', middlewere.isPublic, (req,res) => {
  let userId = req.params.id
  let index = req.query.index

  myController.getUnListing(userId)
  .then(result => {
    if(result.length > 1){
      myController.disticntBarang(result)
      .then(hasilDistinct => {
          res.status(200).send({code : 200, message: "Success", data:hasilDistinct})
      })
    }else{
      res.status(200).send({code : 200, message: "Success", data:result})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.post('/getListingUnListing', middlewere.isPublic, (req,res) => {
  let userId = req.body.id || ""
  let searching = req.body.searching || ""
  let filtering = req.body.filtering || ""
  let sorting = req.body.sorting || ""
  let index = req.body.index || 0

  myController.getListingUnListing(userId, searching, filtering, sorting, index)
  .then(result => {
    if(result.length != 0){
      let i = 0
      asyncLoop(result, function(item, next){
        let id = item.id_barang
        myController.getEcommerce(id)
        .then(getEcommerce => {
          var ecommerce = []
          var status = "Unlisting"
          for(var j = 0; j<getEcommerce.length; j++){
            var tempEcommerce = {}
            tempEcommerce.nama_ecommerce = getEcommerce[j].nama_ecommerce
            tempEcommerce.url = getEcommerce[j].url
            tempEcommerce.status = getEcommerce[j].status

            if(getEcommerce[j].status == 3 && result[i].stok > 0){
              status = "Listing"
            }
            ecommerce.push(tempEcommerce)
          }
          result[i].ecommerce = ecommerce
          result[i].status = status

          myController.getFotoTambahan(id)
          .then(getFoto => {
            result[i].foto_tambahan = getFoto
            myController.getUpdateStock(id)
            .then(getStock => {
              if(getStock.length > 0){
                if(getStock[0].id_status != 3 && getStock[0].id_status_penyebab == 3){
                  result[i].status = "Unlisting"
                  result[i].status_unlisting = "Proses update stok"
                }
                i++
                next()
              }else{
                i++
                next()
              }
            })
          })
        })
      }, function(){
        res.status(200).send({code : 200, message: "Success", data:result})
      })
    }else{
      res.status(200).send({code : 200, message: "Success", data:result})
    }
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.put('/updateStok/:id', middlewere.isPublic, (req,res) => {
  let idBarang = req.params.id

  let stok = req.body.stok

  myController.getById(idBarang)
  .then(getBarang => {
    myController.addStockAfterDelete(idBarang, stok)
    .then(result => {
      if(getBarang[0].stok == stok){
        myController.updateEcommerceAfterSameStock(idBarang)
        .then(hasilAkhir => {
          res.status(200).send({code : 200, message: "Success"})
        })
      }else {
        myController.updateEcommerceAfterAddStock(idBarang, 3)
        .then(hasilAkhir => {
          res.status(200).send({code : 200, message: "Success"})
        })
      }
    })
  })
  .catch(reason => {
    res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
  })
})

route.delete('/hapusBarang/:id', middlewere.isPublic, (req,res) => {
  let idBarang = req.params.id || ""

  if(idBarang != ""){
    myController.hapusBarang(idBarang)
    .then(result => {
      myController.afterHapusBarang(idBarang)
      .then(lastResult => {
        res.status(200).send({code : 200, message: "Success"})
      })
    })
    .catch(reason => {
      res.status(200).send({code : 500, message: "Terjadi kesalahan pada sisi server, silahkan ulangi proses"})
    })
  }else{
    res.status(200).send({code : 403, message: "TParameter yang disertakan tidak sesuai"})
  }
})

module.exports = route
