'use strict'

const routes = require('express').Router()

const category = require('./category')
const subcategory = require('./subcategory')
const user = require('./user')
const transaction = require('./transaction')
const barang = require('./barang')
const fcm = require('./fcm')
const util = require('./util')

routes.use('/category', category)
routes.use('/subcategory', subcategory)
routes.use('/user', user)
routes.use('/transaction', transaction)
routes.use('/barang', barang)
routes.use('/fcm', fcm)
routes.use('/util', util)

module.exports = routes
