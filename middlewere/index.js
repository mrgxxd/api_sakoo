'use strict'

const jwt = require('jsonwebtoken')
const co = require('co')

const config = require('../config')
const code = require('../httpCode')

const SECRET_KEY = config.SECRET_KEY
const handleError = code.handleError

const isPublic = (req, res, next) => {
  let secretkey = req.headers.secretkey
  if(secretkey){
      if(secretkey == SECRET_KEY){
        next()
      }else{
        handleError(res, code.ERR_UNAUTHORIZED)
      }
  }else{
    handleError(res, code.ERR_FORBIDEN)
  }
}

const isLogin = (req, res, next) => {
  let secretkey = req.headers.secretkey
  if(secretkey){
    if(secretkey == SECRET_KEY){
      let token = req.headers.token
      if(token){
        jwt.verify(token, secretkey, function(err, decoded) {
              if(err){
                handleError(res, code.ERR_UNAUTHORIZED)
              }else {
                req.decoded = decoded
                next()
              }
            })
      }else{
        handleError(res, code.ERR_FORBIDEN)
      }
    }else {
      handleError(res, code.ERR_UNAUTHORIZED)
    }
  }else{
    handleError(res, code.ERR_FORBIDEN)
  }
}

const generateToken = (user, secretkey) => {
  return co(function * generateTokenQuery () {
    return new Promise ((resolve, reject) => {
      jwt.sign(user, secretkey, {expiresIn: '1h'}, (err, token) => {
        if(err){
          console.log(err)
          resolve ("")
        }else{
          resolve(token)
        }
      })
    })
  })
}

module.exports = {
  isPublic,
  isLogin,
  generateToken
}
