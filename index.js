'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const morgan = require('morgan')
const compression = require('compression')
const multer = require('multer');
const gm = require('gm');
const jimp = require('jimp');

const config = require('./config')
const routes = require('./routes')

const port = process.env.PORT || 5000

const app = express()

app.use(morgan('dev'))
app.use(compression())
app.use(helmet())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin',"*")
  res.header('Access-Control-Allow-Methods', "GET,POST,PUT,DELETE")
  res.header('Access-Control-Allow-Headers',"Content-Type, secretkey, token")
  next()
})

// API for upload foto
let datafile
let namefile
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'img/profile')
    },
    filename: function (req, file, cb) {
        datafile = file
        namefile = Date.now() +'-'+ file.originalname
        cb(null, namefile)
    }
})

var upload = multer({ storage: storage }).single('profileImage')

app.post('/uploadProfileFoto', (req,res) => {
  upload(req, res, function (err) {
        if (err) {
          res.send({
              success: false,
              message: err
          })
        }
        console.log(datafile)
        res.send({
            success: true,
            message: 'Image uploaded!',
            data : datafile,
            name : 'http://103.195.31.220:5000/getFotoProfile/'+namefile
        })
    })
})

app.get('/getFotoProfile/:idFoto', (req,res,next) => {
  let idFoto = req.params.idFoto
  let size = req.query.size || ""

  function callback(){
    res.sendFile(__dirname + '/img/profile/'+size+idFoto)
  }

  jimp.read(__dirname+'/img/profile/'+idFoto, (err, image) => {
    if(err){
      res.send('err')
    }else{
      if(size == ""){
        res.sendFile(__dirname + '/img/profile/'+idFoto)
      }else if (size == "small") {
        image.contain(92,jimp.AUTO).quality(50).write(__dirname+'/img/profile/small'+idFoto, callback)
      }else if (size == "medium") {
        image.contain(154,jimp.AUTO).quality(65).write(__dirname+'/img/profile/medium'+idFoto, callback)
      }else if (size == "large") {
        image.contain(342,jimp.AUTO).quality(80).write(__dirname+'/img/profile/large'+idFoto, callback)
      }
    }
  })
})

// API for upload foto barang
let datafile2
let namefile2
var storage2 = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'img/barang')
    },
    filename: function (req, file, cb) {
        datafile2 = file
        namefile2 = Date.now() +'-'+ file.originalname
        cb(null, namefile2)
    }
})

var upload2 = multer({ storage: storage2 }).single('barangImage')

app.post('/uploadBarangFoto', (req,res) => {
  console.log(req)
  upload2(req, res, function (err) {
        if (err) {

          res.send({
              success: false,
              message: err
          })
        }
        res.send({
            success: true,
            message: 'Image uploaded!',
            data : datafile2,
            name : 'http://103.195.31.220:5000/getFotoBarang/'+namefile2
        })
    })
})

// app.get('/getFotoBarang/:idFoto', (req,res) => {
//   let idFoto = req.params.idFoto
//   let size = req.query.size || ""
//
//   function callback(){
//     res.sendFile(__dirname + '/img/barang/'+size+idFoto)
//   }
//
//   jimp.read(__dirname+'/img/barang/'+idFoto, (err, image) => {
//     if(err){
//       res.send('err')
//     }else{
//       if(size == ""){
//         res.sendFile(__dirname + '/img/barang/'+idFoto)
//       }else if (size == "small") {
//         image.contain(92,jimp.AUTO).quality(50).write(__dirname+'/img/barang/small'+idFoto, callback)
//       }else if (size == "medium") {
//         image.contain(154,jimp.AUTO).quality(65).write(__dirname+'/img/barang/medium'+idFoto, callback)
//       }else if (size == "large") {
//         image.contain(342,jimp.AUTO).quality(80).write(__dirname+'/img/barang/large'+idFoto, callback)
//       }
//     }
//   })
// })

app.get('/getFotoBarang/:idFoto', (req,res) => {
  let idFoto = req.params.idFoto
  res.sendFile(__dirname + '/img/barang/'+idFoto)
})

// API for upload foto barang
let datafile3
let namefile3
var storage3 = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'img/buktitransfer')
    },
    filename: function (req, file, cb) {
        datafile3 = file
        namefile3 = Date.now() +'-'+ file.originalname
        cb(null, namefile3)
    }
})

var upload3 = multer({ storage: storage3 }).single('invoiceImage')

app.post('/uploadInvoiceFoto', (req,res) => {
  upload3(req, res, function (err) {
        if (err) {

          res.send({
              success: false,
              message: err
          })
        }
        res.send({
            success: true,
            message: 'Image uploaded!',
            data : datafile3,
            name : 'http://103.195.31.220:5000/getFotoInvoice/'+namefile3
        })
    })
})

app.get('/getFotoInvoice/:idFoto', (req,res) => {
  let idFoto = req.params.idFoto
  res.sendFile(__dirname + '/img/barang/'+idFoto)
})

//app.use(config.connection)
app.use('/v1',routes)

app.get('/', (req,res) => {
  res.send('######## SaKoO ########')
})

app.listen(port)
console.log('API_SaKoO v1 ready to use')
