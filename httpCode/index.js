'use strict'

const OK = {code: 200, message: 'Ok'}
const CREATED = {code: 201, message: 'Created'}
const ACCEPTED = {code: 202, message: 'Accepted'}
const ERR_BAD = {code: 400, message: 'Bad request'}
const ERR_UNAUTHORIZED = {code: 401, message: 'Unauthorized'}
const ERR_FORBIDEN = {code: 403, message: 'Forbiden'}
const ERR_NOTFOUND = {code: 404, message: 'Not found'}
const ERR_METHOD = {code: 405, message: 'Method not allowed'}
const ERR_SERVER = {code: 500, message: 'Server error'}

const handleError = (res, error) => {
  res.status(200).send(error)
}

module.exports = {
  handleError,
  OK,
  CREATED,
  ACCEPTED,
  ERR_BAD,
  ERR_UNAUTHORIZED,
  ERR_FORBIDEN,
  ERR_METHOD,
  ERR_NOTFOUND,
  ERR_SERVER
}
